from math import log2, ceil, floor


def get_ascii(blocks):
    return [ord(block) for block in blocks]


def get_char(blocks):
    return [chr(block) for block in blocks]


def modInverse(a, m):
    m0 = m
    y = 0
    x = 1
    if (m == 1):
        return 0

    while (a > 1):
        q = a // m
        t = m
        m = a % m
        a = t
        t = y
        y = x - q * y
        x = t
    if (x < 0):
        x = x + m0
    return x


def encrypt(s, e, N):
    return s**e % N


def decrypt(s, d, N):
    return s**d % N


def bsa_attack(M, e, r, r_inv, d, N):
    attack_message = (M*(r**e)) % N
    dec_attack_message = decrypt(attack_message, d, N)
    obtained_signature = (r_inv*dec_attack_message) % N
    signature = M**d % N
    obtained_message = encrypt(obtained_signature, e, N)
    return attack_message, dec_attack_message, obtained_signature, signature, obtained_message


def is_coprime(a, b):
    if b % a:
        return False
    else:
        return True


def get_blocks(plain_text, no_bits):
    binary = ''.join(['0'*(8-len(bin(ord(char))[2:])) +
                      bin(ord(char))[2:] for char in plain_text])
    binary = [int(binary[k:k+no_bits], 2)
              for k in range(0, len(binary), no_bits)]

    return binary


def compute_no_bits(N):
    # ascii = get_ascii(plain_text)
    # string = ''
    # no_bits = 0
    # for i in ascii:
    #     string += '0'*(8-len(bin(i)[2:]))+bin(i)[2:]
    #     if int(string, 2) > N:
    #         return no_bits
    #     else:
    #         no_bits += 8
    temp = ceil(log2(N))
    no_bits = (temp//8)*8
    return no_bits


def tostring(n, no_bits):
    binary = '0'*(no_bits-len(bin(n)[2:]))+bin(n)[2:]
    dec = [int(binary[k:k+8], 2)
           for k in range(0, len(binary), 8)]
    string = ''
    for d in dec:
        if d != 0:
            string += chr(d)
    return string


if __name__ == "__main__":
    # e = 5
    # p = 229
    # q = 233
    # r = 6
    e = int(input("Enter e: "))
    p = int(input("Enter p: "))
    q = int(input("Enter q: "))
    N = p*q
    r = int(input("Enter r: "))
    c = 1
    exit = False
    while True:
        if not is_coprime(r, N):
            r_inv = modInverse(r, N)
            print('r inverse:', r_inv)
            break
        else:
            print('No modular inverse exists for given r and N')
        r = int(input("Enter r: "))
        c += 1
        if c == 3:
            exit = True
            break
    if exit:
        quit()
    phi_N = (p-1)*(q-1)
    if not is_coprime(e, phi_N):
        d = modInverse(e, phi_N)
        print('calculated d:', d)
    else:
        print('No modular inverse exists for given e:',e,'and phi(N):',phi_N)
        quit()
    input_text = input("Enter Text: ")
    no_bits = compute_no_bits(N)
    if no_bits < 8:
        print('no of bits <8 ')
        quit()
    print('Chars taken at a time: ', no_bits//8)
    M = get_blocks(input_text, no_bits)
    print('Blocks: ', M)
    output = [bsa_attack(m, e, r, r_inv, d, N) for m in M]
    attack_message = [i[0] for i in output]
    dec_attack_message = [i[1] for i in output]
    obtained_signature = [i[2] for i in output]
    signature = [i[3] for i in output]
    obtained_message = [i[4] for i in output]
    print('Attacker sends: ', attack_message)
    print('User decryprts the attack message and sends attacker:', dec_attack_message)
    print('Attacker derives Signature : ', obtained_signature)
    derived_message = ''.join([tostring(i, no_bits) for i in obtained_message])
    print('Derived Message: ', derived_message)
    print('Obtained Signature = original Signature :',
          obtained_signature == signature)

