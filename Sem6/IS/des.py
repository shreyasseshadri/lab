import rkgen as kgen
import sys
import os

pc1 = [56, 48, 40, 32, 24, 16,  8,
		  0, 57, 49, 41, 33, 25, 17,
		  9,  1, 58, 50, 42, 34, 26,
		 18, 10,  2, 59, 51, 43, 35,
		 62, 54, 46, 38, 30, 22, 14,
		  6, 61, 53, 45, 37, 29, 21,
		 13,  5, 60, 52, 44, 36, 28,
		 20, 12,  4, 27, 19, 11,  3
	]
pc2 = [
		13, 16, 10, 23,  0,  4,
		 2, 27, 14,  5, 20,  9,
		22, 18, 11,  3, 25,  7,
		15,  6, 26, 19, 12,  1,
		40, 51, 30, 36, 46, 54,
		29, 39, 50, 44, 32, 47,
		43, 48, 38, 55, 33, 52,
		45, 41, 49, 35, 28, 31
	]

def get_keys(plain_text):
    if len(plain_text)<8:
        print("Input must be a string of length greater than or equal to 8 \n")
    else:
        ch='1'
        if int(ch)==1:
            input_key=plain_text[:8]
        elif int(ch)==2:
            input_key=plain_text[-8:]
        else:
            print("Enter valid choice")  
        bin_text=create_binary_input(input_key)
        return gen_key(''.join(bin_text),'Round-key.txt')

def gen_key(initial_key,file):
    keys=[]
    my_file=open(file,'w')
    loop_input=permute(initial_key,pc1)
    left=loop_input[:28]
    right=loop_input[28:]
    for i in range(16):
        no_rotations=2
        if i==0 or i==1 or i==8 or i==15:
            no_rotations=1
        left=left[no_rotations:]+left[:no_rotations]
        right=right[no_rotations:]+right[:no_rotations]
        temp=left+right
        out=permute(temp,pc2)
        keys.append(out)
        my_file.write(out+'\n')
    my_file.close()
    return keys


def create_binary_input(input_string):
    middle_string=[input_string[k:k+8] for k in range(0,len(input_string),8)]
    string8s=[input_string[k:k+8]+' '*(8-len(input_string[k:k+8])) for k in range(0,len(input_string),8)]
    #print(string8s)
    for ind,block in enumerate(middle_string):
        temp=[]
        if len(block)<8:
            block+=' '*(8-len(block))
        for char in block:
            b=bin(ord(char))[2:]
            b='0'*(8-len(b))+str(b)
            temp.append(b)
        middle_string[ind]=''.join(temp)
    return middle_string,string8s

def permute(sent,table):
    temp=[' ' for i in range(len(table))]
    for ind,val in enumerate(table):
        try:
            temp[ind]=sent[val-1]
        except:
            pass
    return ''.join(temp)

ip = [58, 50, 42, 34, 26, 18, 10, 2,
      60, 52, 44, 36, 28, 20, 12, 4,
      62, 54, 46, 38, 30, 22, 14, 6,
      64, 56, 48, 40, 32, 24, 16, 8,
      57, 49, 41, 33, 25, 17, 9, 1,
      59, 51, 43, 35, 27, 19, 11, 3,
      61, 53, 45, 37, 29, 21, 13, 5,
      63, 55, 47, 39, 31, 23, 15, 7]

fp = [40, 8, 48, 16, 56, 24, 64, 32,
        39, 7, 47, 15, 55, 23, 63, 31,
        38, 6, 46, 14, 54, 22, 62, 30,
        37, 5, 45, 13, 53, 21, 61, 29,
        36, 4, 44, 12, 52, 20, 60, 28,
        35, 3, 43, 11, 51, 19, 59, 27,
        34, 2, 42, 10, 50, 18, 58, 26,
        33, 1, 41, 9, 49, 17, 57, 25]

P = [16, 7, 20, 21, 29, 12, 28, 17,
     1, 15, 23, 26, 5, 18, 31, 10,
     2, 8, 24, 14, 32, 27, 3, 9,
     19, 13, 30, 6, 22, 11, 4, 25]

E = [32, 1, 2, 3, 4, 5,
     4, 5, 6, 7, 8, 9,
     8, 9, 10, 11, 12, 13,
     12, 13, 14, 15, 16, 17,
     16, 17, 18, 19, 20, 21,
     20, 21, 22, 23, 24, 25,
     24, 25, 26, 27, 28, 29,
     28, 29, 30, 31, 32, 1]

sbox = [
         
[[14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7],
 [0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8],
 [4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0],
 [15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13],
],

[[15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10],
 [3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5],
 [0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15],
 [13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9],
],

[[10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8],
 [13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1],
 [13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7],
 [1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12],
],

[[7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15],
 [13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9],
 [10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4],
 [3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14],
],  

[[2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9],
 [14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6],
 [4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14],
 [11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3],
], 

[[12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11],
 [10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8],
 [9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6],
 [4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13],
], 

[[4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1],
 [13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6],
 [1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2],
 [6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12],
],
   
[[13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7],
 [1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2],
 [7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8],
 [2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11],
]
]

def xor(a,b):
    return ''.join([str(int(x)^int(y)) for x,y in zip(a,b)])

def substitution_choice(r_expanded):
    subblocks = [r_expanded[k:k+6] for k in range(0, len(r_expanded), 6)]
    res = ''
    for i in range(len(subblocks)): 
        block = subblocks[i]
        row = int(str(block[0])+str(block[5]),2)
        column = int(''.join([str(x) for x in block[1:][:-1]]),2) 
        val = sbox[i][row][column] 
        binary = str(bin(val)[2:]) 
        res += '0'*(4-len(binary))+binary 
    return res

def bin2string(sent):
    ans=''
    for block in sent:
        temp=[chr(int(block[k:k+8],2)) for k in range(0,len(block),8)]
        ans+=''.join(temp)
    return ans

def encrypt(text_blocks,keys,string8s,file):
    result = []
    output_file=open(file,'w')
    for ind,block in enumerate(text_blocks):
        output_file.write('Block '+str(ind)+'\n')
        output_file.write('Input text : '+string8s[ind]+'\n')
        block = permute(block,ip)
        # output_file.write('After Initial Permutation : '+''.join(block)+'\n')
        left,right = block[:32],block[32:] 
        for i in range(16):
            r_expanded = permute(right,E)
            temp = xor(keys[i], r_expanded)
            temp = substitution_choice(temp) 
            temp = permute(temp, P)
            temp = xor(left, temp)
            left = right
            right = temp
            output_file.write('Round '+str(i+1)+' '+left+right+'\n')
        result.append(permute(right+left,fp))
        output_file.write('Final Permutaion : '+result[-1]+'\n')
    #print('Final Round Result ',result)
    return result
    
if __name__ == "__main__":
    input_text=input("Enter text : ")
    if(len(input_text)>40):
        print('Max size 40 Bytes')
        quit()
    input_key=input("Enter key : ")
    keys=kgen.get_keys(input_key)
    if(not keys):
        print('Key not generated')
        quit()
    binary_input,string8s=create_binary_input(input_text)
    # print(len(input_text)//8+1==len(binary_input))
    final=encrypt(binary_input,keys,string8s,'Output.txt')
    os.system('cat Output.txt')
