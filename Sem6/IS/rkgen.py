def create_binary_input(input_string):
    temp=[]
    for char in input_string:
        b=bin(ord(char))[2:]
        b='0'*(8-len(b))+str(b)
        temp+=[b]
    return temp

pc1 = [56, 48, 40, 32, 24, 16,  8,
		  0, 57, 49, 41, 33, 25, 17,
		  9,  1, 58, 50, 42, 34, 26,
		 18, 10,  2, 59, 51, 43, 35,
		 62, 54, 46, 38, 30, 22, 14,
		  6, 61, 53, 45, 37, 29, 21,
		 13,  5, 60, 52, 44, 36, 28,
		 20, 12,  4, 27, 19, 11,  3
	]
pc2 = [
		13, 16, 10, 23,  0,  4,
		 2, 27, 14,  5, 20,  9,
		22, 18, 11,  3, 25,  7,
		15,  6, 26, 19, 12,  1,
		40, 51, 30, 36, 46, 54,
		29, 39, 50, 44, 32, 47,
		43, 48, 38, 55, 33, 52,
		45, 41, 49, 35, 28, 31
	]

def permute(sent,table,length):
    temp=[' ' for i in range(length)]
    for ind,val in enumerate(table):
        temp[ind]=sent[val]
    return ''.join(temp)

def gen_key(initial_key,file):
    
    keys=[]
    my_file=open(file,'w')
    loop_input=permute(initial_key,pc1,56)
    left=loop_input[:28]
    right=loop_input[28:]
    for i in range(16):
        no_rotations=2
        if i==0 or i==1 or i==8 or i==15:
            no_rotations=1
        left=left[no_rotations:]+left[:no_rotations]
        right=right[no_rotations:]+right[:no_rotations]
        temp=left+right
        out=permute(temp,pc2,48)
        keys.append(out)
        my_file.write("Round "+str(i+1)+" "+out+'\n')
        print("Round ",i+1,out,sep=" ")
    my_file.close()


if __name__ == "__main__":
    plain_text=input("Enter text\n")
    plain_text=plain_text.replace(' ','')
    if len(plain_text)<8:
        print("Input must be a string of length greater than or equal to 8")
    else:
        ch=input("Do you want first 8 bits or last 8 bits as input to keygen?(1/2) \n")
        if int(ch)==1:
            input_key=plain_text[:8]
        elif int(ch)==2:
            input_key=plain_text[-8:]
        else:
            print("Enter valid choice")  
        print("input text ",input_key,sep="")
        bin_text=create_binary_input(input_key)
        print("Binary values of the input to  key generation ",bin_text,sep=" ")
        gen_key(''.join(bin_text),'Round-key.txt')