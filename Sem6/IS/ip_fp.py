def create_binary_input(input_string):
    middle_string=[input_string[k:k+8] for k in range(0,len(input_string),8)]
    string8s=[]
    for ind,block in enumerate(middle_string):
        temp=[]
        if len(block)<8:
            block+=' '*(8-len(block))
        for char in block:
            b=bin(ord(char))[2:]
            b='0'*(8-len(b))+str(b)
            temp.append(b)
        string8s.append(temp)
        middle_string[ind]=''.join(temp)
    return middle_string,string8s

def bin2string(sent):
    ans=''
    for block in sent:
        temp=[chr(int(block[k:k+8],2)) for k in range(0,len(block),8)]
        ans+=''.join(temp)
    return ans

ip=[    57, 49, 41, 33, 25, 17, 9,  1,
		59, 51, 43, 35, 27, 19, 11, 3,
		61, 53, 45, 37, 29, 21, 13, 5,
		63, 55, 47, 39, 31, 23, 15, 7,
		56, 48, 40, 32, 24, 16, 8,  0,
		58, 50, 42, 34, 26, 18, 10, 2,
		60, 52, 44, 36, 28, 20, 12, 4,
		62, 54, 46, 38, 30, 22, 14, 6
	]
fp=[
		39,  7, 47, 15, 55, 23, 63, 31,
		38,  6, 46, 14, 54, 22, 62, 30,
		37,  5, 45, 13, 53, 21, 61, 29,
		36,  4, 44, 12, 52, 20, 60, 28,
		35,  3, 43, 11, 51, 19, 59, 27,
		34,  2, 42, 10, 50, 18, 58, 26,
		33,  1, 41,  9, 49, 17, 57, 25,
		32,  0, 40,  8, 48, 16, 56, 24
	]

def permute(sent,table):
    permuted8s=[]
    permuted_string=[]
    for string in sent:
        temp=[' ' for i in range(64)]
        for ind,val in enumerate(table):
            temp[ind]=string[val]
        temp2=''.join(temp)    
        permuted_string.append(''.join(temp))
        permuted8s.append([temp2[k:k+8] for k in range(0,len(temp2),8)])
    return permuted_string,permuted8s

if __name__ == "__main__":
    input_string=input('Enter string\n')
    middle_string,string8s=create_binary_input(input_string)
    print('Converted binary string :\n',string8s)
    initial_string,initial8s=permute(middle_string,ip)
    print('string after initial permutation: \n',initial8s)
    final_string,final8s=permute(initial_string,fp)
    print('string after final permutation: \n',final8s)
    print('is the string same after initial and final permutation? \n',middle_string==final_string)
    text=bin2string(final_string)
    print('Final text : ')
    print(text)
