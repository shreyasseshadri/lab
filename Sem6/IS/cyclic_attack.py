from math import log2, ceil


def mod_power(x, y, m):
    if x == 1 or y == 0:
        return 1
    if y == 1:
        return x % m
    if y % 2 == 0:
        return (mod_power(x, y//2, m) * mod_power(x, y//2, m)) % m
    else:
        return (x * mod_power(x, y-1, m)) % m
    return


def get_ascii(blocks):
    return [ord(block) for block in blocks]


def get_char(blocks):
    return [chr(block) for block in blocks]


def cyclic_attack(cipher_text, e=13, N=143):
    c_dash = cipher_text
    while True:
        prev = c_dash
        c_dash = c_dash**e % N
        if c_dash == cipher_text:
            break
    # print('cipher text : ', cipher_text, ' plain text : ', prev)
    return prev


def encrypt(blocks, no_bits, e, N):
    pass


def verify(plain_blocks, cipher_text, no_bits, e=13, N=143):
    enc_plain_block = [mod_power(block, e, N) for block in plain_blocks]
    derived_cipher_text = bin_2_text(enc_plain_block, no_bits)
    print('Obtained cipher text after encryption : ',
          ''.join(derived_cipher_text))
    for pt, ct in zip(derived_cipher_text, cipher_text):
        if ct != pt:
            return False
    return True


def text_2_bin(cipher_text, no_bits):
    binary = ''.join(['0'*(8-len(bin(ord(char))[2:])) +
                      bin(ord(char))[2:] for char in cipher_text])
    binary = [int(binary[k:k+no_bits], 2)
              for k in range(0, len(binary), no_bits)]
    return binary


def bin_2_text(plain_block, no_bits):
    binary = ''.join(['0'*(no_bits-len(bin(i)[2:]))+bin(i)[2:]
                      for i in plain_block])
    return [chr(int(binary[k:k+8], 2)) for k in range(0, len(binary), 8)]


if __name__ == "__main__":
    cipher_text = input('Enter cipher text : ')
    e = 3
    N = 51
    # e = int(input('Enter e : '))
    # N = int(input('Enter N : '))
    no_bits = ceil(log2(N))
    cipher_blocks = text_2_bin(cipher_text, no_bits)
    print('Cipher blocks : ', cipher_blocks)
    plain_blocks = [cyclic_attack(char, e, N) for char in cipher_blocks]
    print('Derived plain blocks : ', plain_blocks)
    plain_text = bin_2_text(plain_blocks, no_bits)
    print('Derived plain text :',''.join(plain_text))
    print('Verfying obtained plain text : ',
          verify(plain_blocks, cipher_text, no_bits, e, N))
