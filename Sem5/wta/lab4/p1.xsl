<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
  <body>
  <h2>Concerts info</h2>
  <table border="1">
    <tr>
      <th>name</th>
      <th>genre</th>
      <th>place</th>	
    </tr>
    <xsl:for-each select="ad/concert">
    <tr>
      <td><xsl:value-of select="band"/></td>
      <xsl:if test= "genre='pop'" >
      <td style= "color:red;" ><xsl:value-of select="genre"/></td>
      </xsl:if>
      <xsl:if test= "genre='classical'" >
      <td style="color:blue;"><xsl:value-of select="genre"/></td>
      </xsl:if> 
      <xsl:if test= "genre='instrumental'" >
      <td style="color:green;"><xsl:value-of select="genre"/></td>
      </xsl:if>
      <td><xsl:value-of select="placeinfo/venue"/></td>
    </tr>
    </xsl:for-each>
  </table>
  <!-- <table border="0">
	<tr>
	      <th>name</th>
	      <th>genre</th>
	      <th>place</th>
        <th>price</th>	
	</tr>
	<xsl:for-each select="ad/concert">
			<xsl:if test="price &lt; 100 and placeinfo/venue = 'mangalore'">
				<xsl:for-each select="showtimes/datetime">
					<xsl:if test="month='April'">
						<td style= "color:blue;" ><xsl:value-of select="name"/></td>
						<td style= "color:blue;" ><xsl:value-of select="genre"/></td>
						<td style= "color:blue;" ><xsl:value-of  select="place"/></td>
						<td style= "color:blue;" ><xsl:value-of select="price"/></td>
					</xsl:if>
					<xsl:otherwise>
						<td  ><xsl:value-of select="name"/></td>
						<td ><xsl:value-of select="genre"/></td>
						<td ><xsl:value-of select="place"/></td>
						<td ><xsl:value-of select="price"/></td>
					</xsl:otherwise>
				</xsl:for-each>
			</xsl:if>
			<xsl:otherwise>
				<td  ><xsl:value-of select="name"/></td>
				<td ><xsl:value-of select="genre"/></td>
				<td ><xsl:value-of select="place"/></td>
				<td style="color:red"><xsl:value-of select="price"/></td>
			</xsl:otherwise>
	</xsl:for-each>
  </table> -->

	
	<table border="0" cellspacing="5" cellpadding="5">
     <tr>
      <th>Band Name</th>
      <th>Venue Name</th>
      <th>Venue Address</th>
      <th>Venue Contact</th>
      <th>Dates</th>
      <th>Genre</th>
      <th>Ticket price</th>
      <th>Ticket type</th>
      <th>Discount</th> 
    </tr>

    <xsl:for-each select="ad/concert">
			<xsl:sort select="band"/>
      <xsl:if test="ticket/price &lt; 100">
        <xsl:if test="placeinfo/venue='Mangalore'">
          <xsl:if test="showtimes/datetime/month='April'">
        <tr>
          <td><xsl:value-of select="band"/></td>
          <td><xsl:value-of select="placeinfo/venue"/></td>
          <td><xsl:value-of select="placeinfo/address"/></td>
          <td><xsl:value-of select="placeinfo/contactnumber"/></td>
          <td><xsl:value-of select="showtimes/day"/> <xsl:value-of select="showtimes/month"/> <xsl:value-of select="showtimes/year"/></td>
          <td><xsl:value-of select="genre"/></td>
          <td><xsl:value-of select="ticket/price"/></td>
          <td><xsl:value-of select="ticket/type"/></td>
          <td><xsl:value-of select="discounts"/></td>
        </tr>
      </xsl:if>
      </xsl:if>
      </xsl:if>
       <xsl:if test="discounts!='NA'">
        <tr>
          <td><xsl:value-of select="band"/></td>
          <td bgcolor="blue"><xsl:value-of select="placeinfo/venue"/></td>
          <td bgcolor="blue"><xsl:value-of select="placeinfo/address"/></td>
          <td bgcolor="blue"><xsl:value-of select="placeinfo/contactnumber"/></td>
          <td bgcolor="blue"><xsl:value-of select="showtimes/day"/> <xsl:value-of select="showtimes/month"/> <xsl:value-of select="showtimes/year"/></td>
          <td bgcolor="blue"><xsl:value-of select="genre"/></td>
          <td bgcolor="blue"><xsl:value-of select="ticket/price"/></td>
          <td bgcolor="blue"><xsl:value-of select="ticket/type"/></td>
          <td bgcolor="blue"><xsl:value-of select="discounts"/></td>
        </tr>
      </xsl:if>
      </xsl:for-each>
  </table>	
  </body>
  </html>
</xsl:template>

</xsl:stylesheet> 
