#include<mpi.h>
#include<stdio.h>
int main(int argc,char **argv)
{
    int rank,size,a[10];
    MPI_Status status;
    
    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&size);
    printf("Size: %d \n",size);
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    if(rank==0)
    {   for(int i=0;i<10;i++)a[i]=i;
        MPI_Send(&a,10,MPI_INT,1,1,MPI_COMM_WORLD);
    }
    else if(rank==1)
    {   
        printf("process %d a[2] is :%d\n",rank,a[2]);
        MPI_Recv(&a,10,MPI_INT,0,1,MPI_COMM_WORLD,&status);
        printf("recieved a[2] : %d sender :%d Tag:%d \n",a[2],status.MPI_SOURCE,status.MPI_TAG);
    }
    MPI_Finalize();
}