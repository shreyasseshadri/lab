#include<mpi.h>
#include<stdio.h>
int main(int argc,char ** argv)
{   
    int rank,size,x;
    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&size);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    if(rank==0)
    {   
        scanf("%d",&x);
    }
    MPI_Bcast(&x,1,MPI_INT,0,MPI_COMM_WORLD);
    printf("process %d of %d x is : %d\n",rank,size,x);
    MPI_Finalize();
}