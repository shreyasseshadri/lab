create table physician(employeeid int primary key,name varchar(255),position varchar(255),ssn int);
create table department(departmentid int primary key,name varchar(255),head int,foreign key(head) references physician(employeeid));

create table patient(ssn int primary key,name varchar(255),addresss varchar(255),phone varchar(12), insuranceid int, pcp int,foreign key(pcp) references physician(employeeid));
-----------------------------------------------------	
create table nurse(employeeid int primary key,name varchar(255),position varchar(255),registered char(1),ssn int);


create table appointment(appointmentid int primary key,patient int, prepnurse int default NULL, physician int, start_dt_time varchar(255), end_dt_time varchar(255), examinationroom varchar(255), foreign key(prepnurse) references nurse(employeeid), foreign key(physician) references physician(employeeid),foreign key(patient) references patient(ssn));

create table block(blockfloor int, blockcode int, CONSTRAINT pk_block primary key(blockfloor,blockcode));
-----------------------------------------------------
create table room(roomnumber int primary key, roomtype varchar(255),blockfloor int,blockcode int, unavailable char(1), foreign key(blockfloor,blockcode) references block(blockfloor,blockcode));
----------------------------------------------------
insert into nurse values
(101 , 'Carla Espinosa','Head Nurse','t',111111110),
(102 , 'Laverne Roberts' , 'Nurse', 't', 222222220),
(103 , 'Paul Flowers'    , 'Nurse'      , 'f'          , 333333330);
------------------
insert into physician values
(1 , 'John Dorian'       , 'Staff Internist'              , 111111111),
          (2 , 'Elliot Reid'       , 'Attending Physician'          , 222222222),
          (3 , 'Christopher Turk'  , 'Surgical Attending Physician' , 333333333),
          (4 , 'Percival Cox'      , 'Senior Attending Physician'   , 444444444),
          (5 , 'Bob Kelso'         , 'Head Chief of Medicine'       , 555555555),
          (6 , 'Todd Quinlan'      , 'Surgical Attending Physician' , 666666666),
          (7 , 'John Wen'          , 'Surgical Attending Physician' , 777777777),
          (8 , 'Keith Dudemeister' , 'MD Resident'                  , 888888888),
          (9 , 'Molly Clock'       , 'Attending Psychiatrist'       , 999999999);

insert into department values
  (1 , 'General Medicine' ,    4),
   (2 , 'Surgery'          ,    7),
    (3 , 'Psychiatry'       ,    9);

insert into appointment values	
(13216584 , 100000001 ,       101 ,         1 , '2008-04-24 10:00:00' , '2008-04-24 11:00:00' , 'A'),
      (59871321 , 100000004 ,       NULL    ,         4 , '2008-04-26 10:00:00' , '2008-04-26 11:00:00' , 'C'),
      (69879231 , 100000003 ,       103 ,         2 , '2008-04-26 11:00:00' , '2008-04-26 12:00:00' , 'C'),
      (76983231 , 100000001 ,        NULL  ,         3 , '2008-04-26 12:00:00' , '2008-04-26 13:00:00' , 'C');


insert into room values
(101 , 'Single'   ,          1 ,         1 , 'f'),
       (102 , 'Single'   ,          2 ,         1 , 'f'),
       (212 , 'Single'   ,          3 ,         2 , 'f');

insert into patient values
(100000001 , 'John Smith'        , '42 Foobar Lane'  , '555-0256',68476213 , 1 ),
 (100000002 , 'Grace Ritchie'     , '37 Snafu Drive'     ,   '555-0512', 36546321 ,   2),
 (100000003 , 'Random J. Patient' , '101 Omgbbq Street'  , '555-1204' ,65465421 ,   2),
 (100000004 , 'Dennis Doe'        , '1100 Foobaz Avenue' , '555-2048',68421879 ,   3);

 insert into block values
 (1	      ,1),
(1		,2),
(2		,1),
(2		,2),
(3		,1),
(3		,2);











2 - SELECT physician.name FROM physician INNER JOIN  department WHERE physician.employeeid=department.head;


create table procedur(code int primary key,name varchar(255),cost int);

create table trained_in(physician int,treatment int,certificationdate date, certificationexpires date, CONSTRAINT pk_trained primary key(physician,treatment), foreign key(physician) references physician(employeeid),foreign key(treatment) references procedur(code));

create table undergoes(patient int, procedureID int, procDate varchar(255),physician int,assistingnurse int, CONSTRAINT pk_undergoes primary key(patient, procedureID, procDate), foreign key(patient) references patient(ssn),foreign key(procedureID) references procedur(code),foreign key(physician) references physician(employeeid),foreign key(assistingnurse) references nurse(employeeid));

insert into procedur values
(    1 , 'Reverse Rhinopodoplasty' ,  1500),
(   2 , 'Obtuse Pyloric Recombobulation',  3750),
  (  3 , 'Folded Demiophtalmectomy',  4500),
    (4 , 'Complete Walletectomy' , 10000),
    (5 , 'Obfuscated Dermogastrotomy',  4899);
    

insert into undergoes values
 (100000001 ,         1 , '2008-05-02 00:00:00' ,         3 ,            101),
 (100000001 ,         2 , '2008-05-10 00:00:00' ,         7 ,            101),
 (100000004 ,         4 , '2008-05-13 00:00:00' ,         3 ,            103);



insert into trained_in values
  (       3 ,         1 , '2008-01-01'        , '2008-12-31'),
         (3 ,         2 , '2008-01-01'       , '2008-12-31'),
         (6 ,         2 , '2008-01-01'        , '2008-12-31'),
         (6 ,         5 , '2007-01-01'        , '2007-12-31'),
         (7 ,         1 , '2008-01-01'        , '2008-12-31');
